<?php

namespace app\controllers;

use app\models\Medicos;
use app\models\MedicosSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * MedicoController implements the CRUD actions for Medicos model.
 */
class MedicoController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                ['access' => [
                        'class' => AccessControl::className(),
                        'only' => ['logout', 'index'],
                        'rules' => [
                            [
                                'actions' => ['logout', 'index'],
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            [
                                'allow' => true,
                                'actions' => ['login', 'signup'],
                                'roles' => ['?'],
                            ],
                        ],
                    ],
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Medicos models.
     *
     * @return string
     */
    public function actionIndex() {
        $searchModel = new MedicosSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->setSort([
            'defaultOrder' => ['nombre' => SORT_ASC],
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'numero_empleado' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Medicos model.
     * @param int $numero_empleado Numero Empleado
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($numero_empleado) {
        return $this->render('view', [
                    'model' => $this->findModel($numero_empleado),
        ]);
    }

    /**
     * Creates a new Medicos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Medicos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'numero_empleado' => $model->numero_empleado]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Medicos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $numero_empleado Numero Empleado
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($numero_empleado) {
        $model = $this->findModel($numero_empleado);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'numero_empleado' => $model->numero_empleado]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Medicos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $numero_empleado Numero Empleado
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($numero_empleado) {
        $this->findModel($numero_empleado)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Medicos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $numero_empleado Numero Empleado
     * @return Medicos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($numero_empleado) {
        if (($model = Medicos::findOne(['numero_empleado' => $numero_empleado])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}

<?php

namespace app\controllers;

use app\models\Pacientes;
use app\models\PacientesSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Historiales;
use yii\filters\AccessControl;

/**
 * PacientesController implements the CRUD actions for Pacientes model.
 */
class PacienteController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                ['access' => [
                        'class' => AccessControl::className(),
                        'only' => ['logout', 'index'],
                        'rules' => [
                            [
                                'actions' => ['logout', 'index'],
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            [
                                'allow' => true,
                                'actions' => ['login', 'signup'],
                                'roles' => ['?'],
                            ],
                        ],
                    ],
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Pacientes models.
     *
     * @return string
     */
    public function actionIndex() {
        $searchModel = new PacientesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->setSort([
        'defaultOrder' => ['nombre' => SORT_ASC],
    ]);
//            'query' => Pacientes::find()->orderBy(['nombre' => SORT_ASC]),
//                /*
//                  'pagination' => [
//                  'pageSize' => 50
//                  ],
//                  'sort' => [
//                  'defaultOrder' => [
//                  'nombre' => SORT_ASC,
//                  ]
//                  ],
//                 */
//        ]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pacientes model.
     * @param int $id ID
     * @return int
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $dataProviderHistorial = new ActiveDataProvider(['query' => Historiales::find()->where(['id_paciente' => $id])->orderBy(['fecha' => SORT_DESC]),]);
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'dataProviderHistorial' => $dataProviderHistorial,
        ]);
    }

    /**
     * Creates a new Pacientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Pacientes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pacientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return int|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pacientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionHistorial($id) {
        if (($model = Historiales::findOne(['id_paciente' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Pacientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Pacientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Pacientes::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function getHistorial($id_paciente) {
        if (($model = Historiales::findOne(['id_paciente' => $id_paciente])) != null) {
            return $model;
        }
    }

}

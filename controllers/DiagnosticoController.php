<?php

namespace app\controllers;

use app\models\Diagnosticos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * DiagnosticoController implements the CRUD actions for Diagnosticos model.
 */
class DiagnosticoController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'access' => [
                        'class' => AccessControl::className(),
                        'only' => ['logout', 'index'],
                        'rules' => [
                            [
                                'actions' => ['logout', 'index'],
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                            [
                                'allow' => true,
                                'actions' => ['login', 'signup'],
                                'roles' => ['?'],
                            ],
                        ],
                    ],
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Diagnosticos models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Diagnosticos::find()->orderBy(['fecha_realización' => SORT_DESC]),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'codigo_diagnóstico' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Diagnosticos model.
     * @param int $codigo_diagnóstico Codigo Diagnóstico
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_diagnóstico) {
        return $this->render('view', [
                    'model' => $this->findModel($codigo_diagnóstico),
        ]);
    }

    /**
     * Creates a new Diagnosticos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Diagnosticos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_diagnóstico' => $model->codigo_diagnóstico]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Diagnosticos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_diagnóstico Codigo Diagnóstico
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_diagnóstico) {
        $model = $this->findModel($codigo_diagnóstico);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_diagnóstico' => $model->codigo_diagnóstico]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Diagnosticos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_diagnóstico Codigo Diagnóstico
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_diagnóstico) {
        $this->findModel($codigo_diagnóstico)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Diagnosticos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_diagnóstico Codigo Diagnóstico
     * @return Diagnosticos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_diagnóstico) {
        if (($model = Diagnosticos::findOne(['codigo_diagnóstico' => $codigo_diagnóstico])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}

<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Historiales;
use kartik\mpdf\Pdf;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

//    public function actionReport5() {
//    // get your HTML raw content without any layouts or scripts
//    $content = $this->renderPartial('_reportView');
//    
//    // setup kartik\mpdf\Pdf component
//    $pdf = new Pdf([
//        // set to use core fonts only
//        'mode' => Pdf::MODE_CORE, 
//        // A4 paper format
//        'format' => Pdf::FORMAT_A4, 
//        // portrait orientation
//        'orientation' => Pdf::ORIENT_PORTRAIT, 
//        // stream to browser inline
//        'destination' => Pdf::DEST_BROWSER, 
//        // your html content input
//        'content' => $content,  
//        // format content from your own css file if needed or use the
//        // enhanced bootstrap css built by Krajee for mPDF formatting 
//        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
//        // any css to be embedded if required
//        'cssInline' => '.kv-heading-1{font-size:18px}', 
//         // set mPDF properties on the fly
//        'options' => ['title' => 'Krajee Report Title'],
//         // call mPDF methods on the fly
//        'methods' => [ 
//            'SetHeader'=>['Krajee Report Header'], 
//            'SetFooter'=>['{PAGENO}'],
//        ]
//    ]);
//    
//    // return the pdf output as per the destination setting
//    return $pdf->render(); 
//}
    public function actionReport() {
        /* $model = new Historiales();

          if ($this->request->isPost) {
          if ($model->load($this->request->post()) && $model->save()) {
          return $this->redirect(['view', 'id_paciente' => $model->id_paciente]);
          }
          } else {
          $model->loadDefaultValues();
          }
         */
        // Obtener todos los registros de la tabla "Historiales"
        $historiales = Historiales::find()->all();

        $html = $this->renderPartial('_reportView');
        $pdf = Yii::$app->pdf;
        $mpdf = $pdf->api;
        //$pdf->conent;
        $mpdf->Bookmark('inicio');
        // Recorrer cada registro
        $mpdf->WriteHtml('<table> <tr><th>Id</th><th>Id Paciente</th><th>Id Empleado</th><th>Clínica</th><th>Tratamiento</th><th>Fecha</th></tr>');
        foreach ($historiales as $historial) {
            // Acceder a los atributos del modelo
            /*   echo "ID Paciente: " . $historial->id_paciente . "<br>";
              echo "Nombre: " . $historial->nombre . "<br>";
              echo "Fecha: " . $historial->fecha . "<br>"; */
            // Agrega aquí cualquier otro atributo que quieras mostrar
            $mpdf->WriteHtml('<tr><td> ' . $historial->id . '</td><td>' . $historial->id_paciente . '</td><td>' . $historial->id_empleado . '</td><td>' . $historial->clinica . '</td><td>' . $historial->tratamiento . '</td><td>' . $historial->fecha . '</td></tr>');
        }

        $mpdf->WriteHtml('</table>');
        $pdf->OutPut();
        return $pdf->render();
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

}

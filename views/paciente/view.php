<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\Pacientes $model */
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pacientes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'fecha',
            'numero_habitacion',
        ],
    ])
    ?>

    <h3>Historial Completo del paciente</h3>

    <?=
    GridView::widget([
        'dataProvider' => $dataProviderHistorial,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'clinica',
            'tratamiento',
            [
                'attribute' => 'fecha',
                'format' => ['date', 'php:d/m/Y'], // Formatear como fecha en el formato deseado
                'contentOptions' => ['class' => 'columna_centrada'],
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ])
    ?>


</div>

<?php

use app\models\Pacientes;
use app\models\PacientesSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Pacientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pacientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Pacientes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="pacientes-search">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <?= $form->field($searchModel, 'nombre')->textInput(['placeholder' => 'Buscar por nombre']) ?>

        <div class="form-group">
            <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
            <?= Html::button('Reiniciar', ['class' => 'btn btn-outline-secondary', 'id' => 'resetButton']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'id' => 'tablaPacientes',
        'columns' => [
            'id',
            'nombre',
            [
                'attribute' => 'fecha',
                'format' => ['date', 'php:d/m/Y'], // Formatear como fecha en el formato deseado
                'contentOptions' => ['class' => 'columna_centrada'],
            ],
            'numero_habitacion',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Pacientes $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]);
    ?>


</div>

<?php
$resetScript = <<<JS
document.getElementById('resetButton').addEventListener('click', function() {
    window.location.href = window.location.pathname;
});
JS;
$this->registerJs($resetScript);
?>

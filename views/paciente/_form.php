<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/** @var yii\web\View $this */
/** @var app\models\Pacientes $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pacientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'fecha')->widget(DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd', // Formato de fecha (ajústalo según lo necesites)
        'clientOptions' => [
            'yearRange' => '1980:2100', // Rango de años (últimos 100 años)
            'changeYear' => true, // Permite cambiar el año
            'changeMonth' => true, // Permite cambiar el mes
        ],
        'options' => [
            'class' => 'form-control', // Clase CSS para aplicar estilo Bootstrap
        ],
    ]);
    ?>

    <?= $form->field($model, 'numero_habitacion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

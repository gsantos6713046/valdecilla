<?php

use app\models\Telefono;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Telefonos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telefono-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Telefono', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['attribute' => 'id', 'contentOptions' => ['class' => 'columna_centrada']],
            ['attribute' => 'numero_empleado', 'contentOptions' => ['class' => 'columna_centrada']],
            ['attribute' => 'numero_telefono', 'contentOptions' => ['class' => 'columna_centrada']],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Telefono $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]);
    ?>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Medicos;

/** @var yii\web\View $this */
/** @var app\models\Telefono $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="telefono-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $medicos = Medicos::find()->all();
    $arrayMedicos = array();
    foreach ($medicos as $miMedico) {
        $arrayMedicos[$miMedico->numero_empleado] = $miMedico->nombre;
    }
    ?>

    <?= $form->field($model, 'numero_empleado')->dropDownList($arrayMedicos) ?>

    <?= $form->field($model, 'numero_telefono')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

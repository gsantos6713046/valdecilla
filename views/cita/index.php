<?php

use app\models\Citas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Citas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="citas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Citas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['attribute' => 'numero_Cita', 'contentOptions' => ['class' => 'columna_centrada']],
            ['attribute' => 'id_empleado', 'contentOptions' => ['class' => 'columna_centrada']],
            [
                'attribute' => 'fecha_realización',
                'format' => ['date', 'php:d/m/Y'], // Formatear como fecha en el formato deseado
                'contentOptions' => ['class' => 'columna_centrada'],
            ],
            ['attribute' => 'id_paciente', 'contentOptions' => ['class' => 'columna_centrada']],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Citas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'numero_Cita' => $model->numero_Cita]);
                }
            ],
        ],
    ]);
    ?>


</div>

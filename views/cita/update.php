<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Citas $model */

$this->title = 'Actualizar Citas: ' . $model->numero_Cita;
$this->params['breadcrumbs'][] = ['label' => 'Citas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->numero_Cita, 'url' => ['view', 'numero_Cita' => $model->numero_Cita]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="citas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

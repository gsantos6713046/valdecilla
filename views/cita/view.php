<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Citas $model */
$this->title = $model->numero_Cita;
$this->params['breadcrumbs'][] = ['label' => 'Citas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="citas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'numero_Cita' => $model->numero_Cita], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Eliminar', ['delete', 'numero_Cita' => $model->numero_Cita], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'numero_Cita',
            'id_empleado',
            [
                'attribute' => 'fecha_realización',
                'format' => ['date', 'php:d/m/Y'], // Formatear como fecha en el formato deseado
            ],
            'id_paciente',
        ],
    ])
    ?>

</div>

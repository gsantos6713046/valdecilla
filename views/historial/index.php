<?php

use app\models\Historiales;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use app\controllers\HistorialController;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Historiales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="historiales-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Historiales', ['create'], ['class' => 'btn btn-success']) ?>
    </p

    <p>
        <?= Html::a('Descargar Historiales', ['../site/report'], ['class' => 'btn btn-success', 'target' => '_blank']) ?>
    </p>

    <p>
        <?=
        Html::a('Eliminar Historiales', ['borrar-todos'], ['class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estás seguro de eliminar todos los historiales (esta opción no se puede deshacer',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <div class="table-responsive">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['attribute' => 'id_paciente', 'contentOptions' => ['class' => 'columna_centrada']],
                ['attribute' => 'id_empleado', 'contentOptions' => ['class' => 'columna_centrada']],
                ['attribute' => 'clinica', 'contentOptions' => ['class' => 'truncar_columna']],
                ['attribute' => 'tratamiento', 'contentOptions' => ['class' => 'truncar_columna']],
                [
                    'attribute' => 'fecha',
                    'format' => ['date', 'php:d/m/Y'], // Formatear como fecha en el formato deseado
                    'contentOptions' => ['class' => 'columna_centrada'],
                ],
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Historiales $model, $key, $index, $column) {
                        return Url::toRoute([$action, 'id' => $model->id]);
                    }
                ],
            ],
        ]);
        ?>
    </div>
</div>

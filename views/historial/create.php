<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Historiales $model */

$this->title = 'Crear Historiales';
$this->params['breadcrumbs'][] = ['label' => 'Historiales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="historiales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Historiales $model */
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Historiales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="historiales-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_paciente',
            'id_empleado',
            'clinica',
            'tratamiento',
            [
                'attribute' => 'fecha',
                'format' => ['date', 'php:d/m/Y'], // Formatear como fecha en el formato deseado
            ],
        ],
    ])
    ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Medicos;
use app\models\Pacientes;
use yii\jui\DatePicker;

/** @var yii\web\View $this */
/** @var app\models\Historiales $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="historiales-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $medicos = Medicos::find()->all();
    $arrayMedicos = array();
    foreach ($medicos as $miMedico) {
        $arrayMedicos[$miMedico->numero_empleado] = $miMedico->nombre;
    }
    ?>

    <?php
    $pacientes = Pacientes::find()->all();
    $arrayPacientes = array();
    foreach ($pacientes as $miPaciente) {
        $arrayPacientes[$miPaciente->id] = $miPaciente->nombre;
    }
    ?>

    <?= $form->field($model, 'id_paciente')->dropDownList($arrayPacientes) ?>

    <?= $form->field($model, 'id_empleado')->dropDownList($arrayMedicos) ?>

    <?= $form->field($model, 'clinica')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tratamiento')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'fecha')->widget(DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd', // Formato de fecha (ajústalo según lo necesites)
        'clientOptions' => [
            'yearRange' => '1980:2100', // Rango de años (últimos 100 años)
            'changeYear' => true, // Permite cambiar el año
            'changeMonth' => true, // Permite cambiar el mes
        ],
        'options' => [
            'class' => 'form-control', // Clase CSS para aplicar estilo Bootstrap
        ],
    ]);
    ?>

    <div class="form-group">
    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>

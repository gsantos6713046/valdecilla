<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Medicos $model */

$this->title = 'Crear Medicos';
$this->params['breadcrumbs'][] = ['label' => 'Medicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medicos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Medicos $model */

$this->title = 'Actualizar Medicos: ' . $model->numero_empleado;
$this->params['breadcrumbs'][] = ['label' => 'Medicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->numero_empleado, 'url' => ['view', 'numero_empleado' => $model->numero_empleado]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="medicos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use app\models\Medicos;
use app\models\MedicosSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Medicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medicos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Medicos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <div class="medicos-search">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
        ]);
        ?>

        <?= $form->field($searchModel, 'nombre')->textInput(['placeholder' => 'Buscar por nombre']) ?>

        <div class="form-group">
            <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
            <?= Html::button('Reiniciar', ['class' => 'btn btn-outline-secondary', 'id' => 'resetButton']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['attribute' => 'numero_empleado', 'contentOptions' => ['class' => 'columna_centrada']],
            'nombre',
            ['attribute' => 'especialidad', 'contentOptions' => ['class' => 'columna_centrada']],
            ['attribute' => 'telefono', 'contentOptions' => ['class' => 'columna_centrada']],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Medicos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'numero_empleado' => $model->numero_empleado]);
                }
            ],
        ],
    ]);
    ?>


</div>

<?php
$resetScript = <<<JS
document.getElementById('resetButton').addEventListener('click', function() {
    window.location.href = window.location.pathname;
});
JS;
$this->registerJs($resetScript);
?>

<?php

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'Sobre Nosotros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Valdecilla es un centro de salud muy importante para todo el personal sanitario.
    </p>
    <p>
        El complejo hospitalario está formado por varios conjuntos de edificios, el más importante es:
    <ul>
        <li>El propio Hospital Universitario Marqués de Valdecilla.</li>
        <ol>
            <li>Residencia general (fue derribada en abril del 2008).</li>
            <li>Nueve pabellones de la antigua "Casa de Salud Valdecilla".</li>
            <li>Facultad de Enfermería, de la Universidad de Cantabria.</li>
            <li>Edificio "2 de noviembre", antiguo Edificio de Traumatología.</li>
            <img src="../../assets/img/edificio.jpg" alt="Edificio"/>
            <li>Edificio "Valdecilla Sur", alberga las consultas.</li>
            <li>Edificio "Tres Torres". El sucesor de la Residencia. Fue abierto en su totalidad en mayo de 2016.</li>
            <img src="../../assets/img/residencia.jpg" alt="Residencia" height="168" width="299"/>
        </ol>
        <li>Hospital Cantabria (popularmente denominado Residencia), era el hospital materno-infantil y de los servicios de Medicina Interna, Neurología, Endocrionología, Reumatología y Neurología. Fue cerrado el 15 de mayo de 2016</li>
        <li>Centro de Especialidades Vargas, consultas externas.</li>

    </ul>
</p>


</div>

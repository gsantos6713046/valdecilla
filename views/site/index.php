<?php
/** @var yii\web\View $this */
$this->title = 'Proyecto Final Grado Superior';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Bienvenido al CEC de Valdecilla</h1>
        <p class="foto">
            <img id="fotoPortada"src="../../assets/img/valdecilla.jpg" alt="Valdecilla"/>

        </p>

    </div>

</div>

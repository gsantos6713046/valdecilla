<?php

use app\models\Diagnosticos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Diagnosticos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diagnosticos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Diagnosticos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['attribute' => 'codigo_diagnóstico', 'contentOptions' => ['class' => 'columna_centrada']],
            'tipo_diagnostico',
            [
                'attribute' => 'fecha_realización',
                'format' => ['date', 'php:d/m/Y'], // Formatear como fecha en el formato deseado
                'contentOptions' => ['class' => 'columna_centrada'],
            ],
            ['attribute' => 'complicaciones', 'contentOptions' => ['class' => 'columna_centrada']],
            ['attribute' => 'numero_empleado', 'contentOptions' => ['class' => 'columna_centrada']],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Diagnosticos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_diagnóstico' => $model->codigo_diagnóstico]);
                }
            ],
        ],
    ]);
    ?>


</div>

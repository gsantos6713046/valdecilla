<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Medicos;

/** @var yii\web\View $this */
/** @var app\models\Diagnosticos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="diagnosticos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $medicos = Medicos::find()->all();
    $arrayMedicos = array();
    foreach ($medicos as $miMedico) {
        $arrayMedicos[$miMedico->numero_empleado] = $miMedico->nombre;
    }
    ?>

    <?= $form->field($model, 'tipo_diagnostico')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_realización')->widget(DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd', // Formato de fecha (ajústalo según lo necesites)
        'clientOptions' => [
            'yearRange' => '1980:2100', // Rango de años (últimos 100 años)
            'changeYear' => true, // Permite cambiar el año
            'changeMonth' => true, // Permite cambiar el mes
        ],
        'options' => [
            'class' => 'form-control', // Clase CSS para aplicar estilo Bootstrap
        ],
    ]); ?>

    <?= $form->field($model, 'complicaciones')->textInput() ?>

    <?= $form->field($model, 'numero_empleado')->dropDownList($arrayMedicos) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

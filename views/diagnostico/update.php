<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Diagnosticos $model */

$this->title = 'Actualizar Diagnosticos: ' . $model->codigo_diagnóstico;
$this->params['breadcrumbs'][] = ['label' => 'Diagnosticos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_diagnóstico, 'url' => ['view', 'codigo_diagnóstico' => $model->codigo_diagnóstico]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="diagnosticos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Diagnosticos $model */

$this->title = $model->codigo_diagnóstico;
$this->params['breadcrumbs'][] = ['label' => 'Diagnosticos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="diagnosticos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'codigo_diagnóstico' => $model->codigo_diagnóstico], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'codigo_diagnóstico' => $model->codigo_diagnóstico], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_diagnóstico',
            'tipo_diagnostico',
            [
                'attribute' => 'fecha_realización',
                'format' => ['date', 'php:d/m/Y'], // Formatear como fecha en el formato deseado
            ],
            'complicaciones',
            'numero_empleado',
        ],
    ]) ?>

</div>

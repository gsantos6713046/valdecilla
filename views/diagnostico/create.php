<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Diagnosticos $model */

$this->title = 'Crear Diagnosticos';
$this->params['breadcrumbs'][] = ['label' => 'Diagnosticos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diagnosticos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "historiales".
 *
 * @property int $id
 * @property int|null $id_paciente
 * @property int|null $id_empleado
 * @property string|null $clinica
 * @property string|null $tratamiento
 * @property string|null $fecha
 *
 * @property Medicos $empleado
 * @property Pacientes $paciente
 */
class Historiales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'historiales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_paciente', 'id_empleado'], 'integer'],
            [['fecha'], 'safe'],
            [['clinica'], 'string', 'max' => 30],
            [['tratamiento'], 'string', 'max' => 255],
            [['id_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Medicos::class, 'targetAttribute' => ['id_empleado' => 'numero_empleado']],
            [['id_paciente'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['id_paciente' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paciente' => 'Id Paciente',
            'id_empleado' => 'Id Empleado',
            'clinica' => 'Clinica',
            'tratamiento' => 'Tratamiento',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Empleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(Medicos::class, ['numero_empleado' => 'id_empleado']);
    }

    /**
     * Gets query for [[Paciente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaciente()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'id_paciente']);
    }
}

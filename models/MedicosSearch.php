<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Medicos;

class MedicosSearch extends Medicos {

    public function rules() {
        return [
            [['numero_empleado', 'especialidad'], 'integer'],
            [['nombre', 'telefono'], 'safe'],
        ];
    }

    public function search($params) {
        $query = Medicos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // Si la validación falla, devuelve los resultados no filtrados
            return $dataProvider;
        }

        // Condiciones de filtrado
        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }

}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "medicos".
 *
 * @property int $numero_empleado
 * @property string|null $nombre
 * @property string|null $especialidad
 * @property string|null $telefono
 *
 * @property Citas $citas
 * @property Diagnosticos[] $diagnosticos
 * @property Historiales[] $historiales
 * @property Telefono $telefono0
 * @property Telefono $telefono1
 */
class Medicos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'medicos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'especialidad'], 'string', 'max' => 30],
            [['telefono'], 'string', 'max' => 12],
            [['nombre'], 'unique'],
            [['telefono'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numero_empleado' => 'Número Empleado',
            'nombre' => 'Nombre',
            'especialidad' => 'Especialidad',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * Gets query for [[Citas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasOne(Citas::class, ['nombre_medico' => 'nombre']);
    }

    /**
     * Gets query for [[Diagnosticos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiagnosticos()
    {
        return $this->hasMany(Diagnosticos::class, ['numero_empleado' => 'numero_empleado']);
    }

    /**
     * Gets query for [[Historiales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHistoriales()
    {
        return $this->hasMany(Historiales::class, ['nombre_medico' => 'nombre']);
    }

    /**
     * Gets query for [[Telefono0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefono0()
    {
        return $this->hasOne(Telefono::class, ['numero_empleado' => 'numero_empleado']);
    }

    /**
     * Gets query for [[Telefono1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefono1()
    {
        return $this->hasOne(Telefono::class, ['numero_telefono' => 'telefono']);
    }
}

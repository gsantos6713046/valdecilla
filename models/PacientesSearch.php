<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pacientes;

class PacientesSearch extends Pacientes {

    public function rules() {
        return [
            [['id', 'numero_habitacion'], 'integer'],
            [['nombre', 'fecha'], 'safe'],
        ];
    }

    public function search($params) {
        $query = Pacientes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // Si la validación falla, devuelve los resultados no filtrados
            return $dataProvider;
        }

        // Condiciones de filtrado
        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }

}

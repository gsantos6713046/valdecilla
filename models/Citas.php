<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "citas".
 *
 * @property int $numero_Cita
 * @property string|null $id_empleado
 * @property string|null $fecha_realización
 * @property string|null $id_paciente
 *
 * @property Medicos $numero_empleado
 * @property Pacientes $id
 */
class Citas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'citas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_realización'], 'safe'],
            [['id_empleado'], 'string', 'max' => 30],
            [['id_paciente'], 'string', 'max' => 20],
            [['id_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Medicos::class, 'targetAttribute' => ['id_empleado' => 'numero_empleado']],
            [['id_paciente'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['id_paciente' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numero_Cita' => 'Número Cita',
            'id_empleado' => 'Id Empleado',
            'fecha_realización' => 'Fecha Realización',
            'id_paciente' => 'Id Paciente',
        ];
    }

    /**
     * Gets query for [[IdEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpleado()
    {
        return $this->hasOne(Medicos::class, ['numero_empleado' => 'id_empleado']);
    }

    /**
     * Gets query for [[IdPaciente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPaciente()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'id_paciente']);
    }
}

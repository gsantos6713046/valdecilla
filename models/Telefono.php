<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefono".
 *
 * @property int $id
 * @property int|null $numero_empleado
 * @property string|null $numero_telefono
 *
 * @property Medicos $numeroEmpleado
 */
class Telefono extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefono';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_empleado'], 'integer'],
            [['numero_telefono'], 'string', 'max' => 12],
            [['numero_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Medicos::class, 'targetAttribute' => ['numero_empleado' => 'numero_empleado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero_empleado' => 'Número Empleado',
            'numero_telefono' => 'Número Telefono',
        ];
    }

    /**
     * Gets query for [[NumeroEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNumeroEmpleado()
    {
        return $this->hasOne(Medicos::class, ['numero_empleado' => 'numero_empleado']);
    }
}

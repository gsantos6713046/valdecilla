<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pacientes".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $fecha
 * @property int|null $numero_habitacion
 *
 * @property Citas[] $citas
 * @property Historiales $historiales
 */
class Pacientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pacientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['numero_habitacion'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'fecha' => 'Fecha',
            'numero_habitacion' => 'Número Habitacion',
        ];
    }

    /**
     * Gets query for [[Citas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Citas::class, ['id_paciente' => 'id']);
    }

    /**
     * Gets query for [[Historiales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHistoriales()
    {
        return $this->hasOne(Historiales::class, ['id_paciente' => 'id']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "diagnosticos".
 *
 * @property int $codigo_diagnóstico
 * @property string|null $tipo_diagnostico
 * @property string|null $fecha_realización
 * @property int|null $complicaciones
 * @property int|null $numero_empleado
 *
 * @property Medicos $numeroEmpleado
 */
class Diagnosticos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'diagnosticos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_realización'], 'safe'],
            [['complicaciones', 'numero_empleado'], 'integer'],
            [['tipo_diagnostico'], 'string', 'max' => 50],
            [['numero_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Medicos::class, 'targetAttribute' => ['numero_empleado' => 'numero_empleado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_diagnóstico' => 'Codigo Diagnóstico',
            'tipo_diagnostico' => 'Tipo Diagnóstico',
            'fecha_realización' => 'Fecha Realización',
            'complicaciones' => 'Complicaciones',
            'numero_empleado' => 'Número Empleado',
        ];
    }

    /**
     * Gets query for [[NumeroEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNumeroEmpleado()
    {
        return $this->hasOne(Medicos::class, ['numero_empleado' => 'numero_empleado']);
    }
}
